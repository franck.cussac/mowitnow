package mowitnow

import scala.util.Try

class Tondeuse(val x: Int, val y: Int, val orientation: Cardinal) {
  def avancer(size: Size): Tondeuse = orientation match {
    case North => if (y < size.y) new Tondeuse(x, y + 1, orientation) else this
    case East => if (x < size.x) new Tondeuse(x + 1, y, orientation) else this
    case West => if (x > 0) new Tondeuse(x - 1, y, orientation) else this
    case South => if (y > 0) new Tondeuse(x, y - 1, orientation) else this
  }

  def tournerDroite: Tondeuse = orientation match {
    case North => new Tondeuse(x, y, East)
    case East => new Tondeuse(x, y, South)
    case West => new Tondeuse(x, y, North)
    case South => new Tondeuse(x, y, West)
  }

  def tournerGauche: Tondeuse = orientation match {
    case North => new Tondeuse(x, y, West)
    case East => new Tondeuse(x, y, North)
    case West => new Tondeuse(x, y, South)
    case South => new Tondeuse(x, y, East)
  }

  def deplacerTondeuse(size: Size, instructions: Instructions): Tondeuse = {
    def actionTondeuse(c: Char): Tondeuse = c match {
      case 'D' => this.tournerDroite
      case 'G' => this.tournerGauche
      case 'A' => this.avancer(size)
    }

    if (instructions.str.isEmpty) this
    else actionTondeuse(instructions.str.head).deplacerTondeuse(size, Instructions(instructions.str.tail))
  }

  override def toString: String = s"(posX = $x, posY = $y, orientation = $orientation)"
}

object Tondeuse {
  def deplacerTout(size: Size, list: List[Try[(Tondeuse, Instructions)]]): List[Try[Tondeuse]] = {
    list.map(mouvementTry => mouvementTry.map {
      case (tondeuse, instructions) => tondeuse.deplacerTondeuse(size, instructions)
    })
  }
}