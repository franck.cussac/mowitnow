package mowitnow

import java.util.logging.Logger

import scala.util.{Failure, Success, Try}


object MowItNow {
  private val log = Logger.getLogger("MowItNow")

  def main(args: Array[String]): Unit = {
    val input =
      """5 5
        |1 2 N
        |GAGAGAGAA
        |sdf dsf sdcx
        |AADAADADDA
        |3 3 E
        |fdgs sfdg
        |3 3 E
        |AADAADADDA
        |3 3 E dsf
        |AADAADADDA""".stripMargin

    val (size, listTondeuses) = Parser.parser(input)

    val result = Tondeuse.deplacerTout(size, listTondeuses)

    printResult(result)
  }

  private def printResult(result: List[Try[Tondeuse]]) = {
    result.foreach {
      case Success(t) => println(s"${t.x} ${t.y} ${t.orientation.toString.head}")
      case Failure(e) => log.warning(e.toString)
    }
  }
}
