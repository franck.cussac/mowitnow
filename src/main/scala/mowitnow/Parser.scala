package mowitnow

import java.util.logging.Logger

import scala.util.{Failure, Success, Try}


trait Cardinal
case object North extends Cardinal
case object East extends Cardinal
case object West extends Cardinal
case object South extends Cardinal

case class Size(x: Int, y: Int)
case class Instructions(str: String)

case class BadArguments(msg: String) extends Error(msg)

object Parser {
  private val log = Logger.getLogger("MowItNow")

  def parser(input: String): (Size, List[Try[(Tondeuse, Instructions)]]) = {
    val lignes = input.split("\n").toList

    val size = parseSize(lignes.head)

    val (lignesTondeuse, lignesInstruction)= lignes.tail.zipWithIndex.partition(x => x._2 % 2 == 0)

    val listeTondeuses = lignesTondeuse.map(x => parseTondeuse(x._1))
    val listeInstructions = lignesInstruction.map(x => parseInstructions(x._1))

    if (listeTondeuses.length > listeInstructions.length && listeTondeuses.last.isSuccess) {
      log.warning(s"La dernière tondeuse ${listeTondeuses.last.get} n'ayant pas d'instructions n'est pas retenue car elle finira sa course là où elle l'a commencée")
    }

    (size, listeTondeuses.zip(listeInstructions).map {
      case (Success(tondeuse), Success(instructions)) => Success((tondeuse, instructions))
      case (Failure(tondeuse), _) => Failure(tondeuse)
      case (Success(t), Failure(instructions)) => Failure(BadArguments(s"${instructions.getMessage}. Tondeuse : $t"))
    })
  }

  private def parseInstructions(instructions: String): Try[Instructions] = instructions.split(" ") match {
    case array if array.length < 2 => Success(Instructions(instructions))
    case _ => Failure(BadArguments(s"""Tondeuse ignorée : "$instructions" ne respecte pas le format de liste d'instructions"""))
  }

  private def parseTondeuse(pos: String): Try[Tondeuse] = pos.split(" ") match {
    case array if array.length < 3 => Failure(BadArguments(s"""Tondeuse ignorée : les arguments "$pos" sont insuffisant pour la définir"""))
    case Array(x, y, orientation) => Try(new Tondeuse(x.toInt, y.toInt, parseCardinal(orientation))) match {
      case Failure(_) => Failure(BadArguments(s"""Tondeuse ignorée : les arguments "$pos" sont invalides pour la définir"""))
      case Success(v) => Success(v)
    }
    case _ => Failure(BadArguments(s"""Tondeuse ignorée : les arguments "$pos" sont trop nombreux pour la définir"""))
  }

  private def parseSize(head: String): Size = head.split(" ") match {
    case Array() => throw BadArguments("La ligne qui définit la taille du terrain est vide")
    case Array(_) => throw BadArguments("Pas assez d'argument pour définir la taille du terrain (1 au lieu de 2)")
    case Array(x, y) => Size(x.toInt, y.toInt)
    case _ => throw BadArguments("Seulement 2 arguments sont nécessaires pour définir la taille du terrain")
  }

  private def parseCardinal(str: String): Cardinal = str match {
    case "N" => North
    case "E" => East
    case "W" => West
    case "S" => South
    case _ => throw BadArguments("le point cardinal donné n'existe pas")
  }
}
