package mowitnow

import org.scalatest._

import scala.util.{Failure, Success}

class MainTest extends FunSuite with GivenWhenThen with Matchers {

  test("parser 1 tondeuse correcte") {
    Given("chaine de caractère pour lancer le programme")
    val input =
      """5 5
        |1 2 N
        |GAGAGAGAA""".stripMargin

    When("on parse les données en entrée")
    val (resultSize, list) = Parser.parser(input)

    Then("je récupère la taille du terrain, la position de la tondeuse et la liste d'instruction")
    resultSize shouldEqual Size(5, 5)
    list.head.get._1.x shouldEqual 1
    list.head.get._1.y shouldEqual 2
    list.head.get._1.orientation shouldEqual North
    list.head.get._2 shouldEqual Instructions("GAGAGAGAA")
  }

  test("parser 3 tondeuses correctes") {
    Given("chaine de caractère pour lancer le programme")
    val input =
      """5 5
        |1 2 N
        |GAGAGAGAA
        |3 3 E
        |AADAADADDA
        |4 4 W
        |GADAADDDAGAA""".stripMargin

    When("on parse les données en entrée")
    val (resultSize, list) = Parser.parser(input)

    Then("je récupère la taille du terrain, et les infos de toutes mes tondeuses")
    resultSize shouldEqual Size(5, 5)

    list.head.get._1.x shouldEqual 1
    list.head.get._1.y shouldEqual 2
    list.head.get._1.orientation shouldEqual North
    list.head.get._2 shouldEqual Instructions("GAGAGAGAA")

    list(1).get._1.x shouldEqual 3
    list(1).get._1.y shouldEqual 3
    list(1).get._1.orientation shouldEqual East
    list(1).get._2 shouldEqual Instructions("AADAADADDA")

    list(2).get._1.x shouldEqual 4
    list(2).get._1.y shouldEqual 4
    list(2).get._1.orientation shouldEqual West
    list(2).get._2 shouldEqual Instructions("GADAADDDAGAA")
  }

  test("parser 3 tondeuses avec une erreur au milieu") {
    Given("chaine de caractère pour lancer le programme")
    val input =
      """5 5
        |1 2 N
        |GAGAGAGAA
        |sdf dsf sdcx
        |AADAADADDA
        |4 4 W
        |GADAADDDAGAA""".stripMargin

    When("on parse les données en entrée")
    val (resultSize, list) = Parser.parser(input)

    Then("je récupère la taille du terrain, et les infos de toutes mes tondeuses avec dedans une erreur")
    resultSize shouldEqual Size(5, 5)

    list.head.get._1.x shouldEqual 1
    list.head.get._1.y shouldEqual 2
    list.head.get._1.orientation shouldEqual North
    list.head.get._2 shouldEqual Instructions("GAGAGAGAA")

    list(1).isFailure shouldEqual true

    list(2).get._1.x shouldEqual 4
    list(2).get._1.y shouldEqual 4
    list(2).get._1.orientation shouldEqual West
    list(2).get._2 shouldEqual Instructions("GADAADDDAGAA")
  }

  test("parser de mauvaises données en input génère une erreur") {
    Given("une chaine de caractère random")
    val input = "lsdkjf lskdjf"

    When("on parse les données en entrée")
    a [NumberFormatException] should be thrownBy Parser.parser(input)
  }

  test("parser une chaine vide") {
    Given("une chaine de caractère vide")
    val input = ""

    When("on parse les données en entrée")
    a [BadArguments] should be thrownBy Parser.parser(input)
  }

  test("parser une liste avec aucune instruction pour la dernière tondeuse") {
    Given("chaine de caractère pour lancer le programme")
    val input =
      """5 5
        |1 2 N
        |GAGAGAGAA
        |sdf dsf sdcx
        |AADAADADDA
        |4 4 W""".stripMargin

    When("on parse les données en entrée")
    val (resultSize, list) = Parser.parser(input)

    Then("je récupère que 2 tondeuses")
    list.length shouldEqual 2
  }

  test("déplacer la tondeuse sans qu'elle touche le bord du terrain") {
    Given("une taille pour le champs, une position pou rla tondeuse et une liste d'instruction")
    val size = Size(5, 5)
    val tondeuse = new Tondeuse(1, 2, North)
    val instructions = Instructions("GAGAGAGAA")

    When("on déplace la tondeuse")
    val result = tondeuse.deplacerTondeuse(size, instructions)

    Then("on obtient la position finale")
    result.x shouldEqual 1
    result.y shouldEqual 3
    result.orientation shouldEqual North
  }

  test("déplacer la tondeuse en la faisant toucher le bord du terrain") {
    Given("une taille pour le champs, une position pou rla tondeuse et une liste d'instruction")
    val size = Size(5, 5)
    val tondeuse = new Tondeuse(1, 2, North)
    val instructions = Instructions("GAAAGAGAGAAAAAAADDA")

    When("on déplace la tondeuse")
    val result = tondeuse.deplacerTondeuse(size, instructions)

    Then("on obtient la position finale")
    result.x shouldEqual 1
    result.y shouldEqual 4
    result.orientation shouldEqual South
  }

  test("déplacer une liste de tondeuse avec une erreur dedans") {
    Given("une liste de tondeuses avec les instructions de déplacement et une erreur")
    val list = List(
      Success((new Tondeuse(1, 2, North), Instructions("GAGAGAGAA"))),
      Failure(BadArguments("fail tondeuse")),
      Success((new Tondeuse(3, 3, East), Instructions("AADAADADDA")))
    )
    val size = Size(5, 5)

    When("on exécute les déplacements pour la liste de tondeuses")
    val result = Tondeuse.deplacerTout(size, list)

    Then("on retrouve tous nos déplacements")
    result.head.get.x shouldEqual 1
    result.head.get.y shouldEqual 3
    result.head.get.orientation shouldEqual North

    result(1).isFailure shouldEqual true

    result.last.get.x shouldEqual 5
    result.last.get.y shouldEqual 1
    result.last.get.orientation shouldEqual East
  }
}
